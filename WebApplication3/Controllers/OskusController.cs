﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3;

namespace WebApplication3.Controllers
{
    public class OskusController : Controller
    {
        private InimesedPadevusedEntities db = new InimesedPadevusedEntities();

        // GET: Oskus
        public ActionResult Index()
        {
            return View(db.Oskused.ToList());
        }

        // GET: Oskus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oskus oskus = db.Oskused.Find(id);
            if (oskus == null)
            {
                return HttpNotFound();
            }
            return View(oskus);
        }

        // GET: Oskus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Oskus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nimetus")] Oskus oskus)
        {
            if (ModelState.IsValid)
            {
                db.Oskused.Add(oskus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(oskus);
        }

        // GET: Oskus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oskus oskus = db.Oskused.Find(id);
            if (oskus == null)
            {
                return HttpNotFound();
            }
            return View(oskus);
        }

        // POST: Oskus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nimetus")] Oskus oskus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oskus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oskus);
        }

        // GET: Oskus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oskus oskus = db.Oskused.Find(id);
            if (oskus == null)
            {
                return HttpNotFound();
            }
            return View(oskus);
        }

        // POST: Oskus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oskus oskus = db.Oskused.Find(id);
            db.Oskused.Remove(oskus);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
