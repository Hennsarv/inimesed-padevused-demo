﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3;

namespace WebApplication3.Controllers
{
    public class RollsController : Controller
    {
        private InimesedPadevusedEntities db = new InimesedPadevusedEntities();

        // GET: Rolls
        public ActionResult Index()
        {
            return View(db.Rollid.ToList());
        }

        // GET: Rolls/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roll roll = db.Rollid.Find(id);
            if (roll == null)
            {
                return HttpNotFound();
            }
            return View(roll);
        }

        // GET: Rolls/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Rolls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nimetus")] Roll roll)
        {
            if (ModelState.IsValid)
            {
                db.Rollid.Add(roll);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(roll);
        }

        // GET: Rolls/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roll roll = db.Rollid.Find(id);
            if (roll == null)
            {
                return HttpNotFound();
            }
            return View(roll);
        }

        // POST: Rolls/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nimetus")] Roll roll)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roll).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(roll);
        }

        // GET: Rolls/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Roll roll = db.Rollid.Find(id);
            if (roll == null)
            {
                return HttpNotFound();
            }
            return View(roll);
        }

        // POST: Rolls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Roll roll = db.Rollid.Find(id);
            db.Rollid.Remove(roll);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
